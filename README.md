# Topicset Book Map Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Script Features
This sample script compares two DITA Book Maps, book1/main.ditamap and book2/main.ditamap, using the 'topic set', 'map pair' and 'unified map' map result structures. For concept details see: [Topicset Book Map documentation](https://docs.deltaxml.com/dita-compare/latest/topicset-book-map-sample-3801194.html).

## Running the sample via the Ant build script

The sample comparison can be run via an Apache Ant build script using the following commands.

| Command | Actions Performed |
| --- | --- |
| ant run | Run all comparisons.
| ant run-topic-set-A | Run the 'topic set' comparison (where the first 'A' input is being used as the 'result origin'). |
| ant run-map-pair-B | Run the 'map pair' comparison (where the second 'B' input is being used as the 'result origin'). |
| ant run-unified-map-B | Run the 'unified map' comparison (where the second 'B' input is being used as the 'result origin'). |
| ant clean | Remove the generate output. |

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    java -jar ../../deltaxml-dita.jar compare map book1/main.ditamap book2/main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set
   
The 'map pair' or 'unified map' comparisons can be run in a similar manner to the 'topic set', but in this case the map-result-orign is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B.

To clean up the sample directory, run the following Ant command.

	ant clean